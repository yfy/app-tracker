package com.example.yfy.apptracker;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by yfy on 25.08.2015.
 */
public class MyPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return MyFragment.newInstance(TrackManager.DAILY);
            case 1:
                return MyFragment.newInstance(TrackManager.WEEKLY);
            case 2:
                return MyFragment.newInstance(TrackManager.MONTHLY);
            default:
                return MyFragment.newInstance(TrackManager.DAILY);
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
