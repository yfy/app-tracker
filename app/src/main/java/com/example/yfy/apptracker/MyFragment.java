package com.example.yfy.apptracker;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MyFragment extends Fragment {
    private int daysAgo;
    private Activity activityContext;
    private final static String daysAgoKey = "days_ago";

    public static MyFragment newInstance(int daysAgo) {
        MyFragment fragment = new MyFragment();
        Bundle args = new Bundle();
        args.putInt(daysAgoKey, daysAgo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityContext = getActivity();
        daysAgo = getArguments().getInt(daysAgoKey, 1);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my, container, false);
        ListView listView = (ListView) view.findViewById(R.id.list);
        TrackManager manager = new TrackManager(activityContext);
        Log.d("Days Ago:", String.valueOf(daysAgo));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_list_item_1, manager.getUsageDataSince(daysAgo));
        listView.setAdapter(adapter);
        return view;
    }

}
