package com.example.yfy.apptracker;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import java.util.*;

/**
 * Created by yfy on 24.08.2015.
 */
public class TrackManager {

    private Activity mActivity;
    public static final int DAILY = 1;
    public static final int WEEKLY = 7;
    public static final int MONTHLY = 30;

    public TrackManager(Activity activity) {
        mActivity = activity;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    public List<String> getUsageDataSince(int daysAgo) {
        UsageStatsManager usageStatsManager = (UsageStatsManager) mActivity.getSystemService(Context.USAGE_STATS_SERVICE);
        Map<String, UsageStats> usageStatsMap = usageStatsManager.queryAndAggregateUsageStats(getDateDaysAgo(daysAgo), Calendar.getInstance().getTimeInMillis());
        Map<String, Long> packageNameTimeMap = new LinkedHashMap<>();

        for (Map.Entry<String, UsageStats> entry :
                usageStatsMap.entrySet()) {
            packageNameTimeMap.put(entry.getKey(), entry.getValue().getTotalTimeInForeground());
        }
        Map<String, Long> sortedPackageNameTimeMap = sortByComparator(packageNameTimeMap);
        List<String> usageStatsData = new ArrayList<>();

        for (Map.Entry<String, Long> entry :
                sortedPackageNameTimeMap.entrySet()) {
            usageStatsData.add("Package Name: " + entry.getKey() + ", Time used: " + entry.getValue());
        }

        return usageStatsData;
    }

    private long getDateDaysAgo(int daysAgo) {
        Calendar oneWeekBefore = Calendar.getInstance();
        oneWeekBefore.add(Calendar.DATE, -1*daysAgo);
        return oneWeekBefore.getTimeInMillis();
    }

    private Map<String, Long> sortByComparator(Map<String, Long> unsortedMap) {
        List<Map.Entry<String, Long>> list = new LinkedList<>(unsortedMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Long>>() {
            @Override
            public int compare(Map.Entry<String, Long> lhs, Map.Entry<String, Long> rhs) {
                return rhs.getValue().compareTo(lhs.getValue());
            }
        });

        Map<String, Long> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, Long> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
}
